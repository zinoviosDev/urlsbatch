package fr.marcdev.urlsbatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import fr.marcdev.urlsbatch.service.MainService;

@SpringBootApplication
@Profile("!test")
public class UrlsBatchApplication {
	
	@Autowired MainService mainService;
	
	public static void main(String[] args) {
		SpringApplication.run(UrlsBatchApplication.class, args);		
	}
}

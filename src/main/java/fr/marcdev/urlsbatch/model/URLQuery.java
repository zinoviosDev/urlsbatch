package fr.marcdev.urlsbatch.model;

import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Class {@code URLQuery} represents a Uniform Resource
 * Locator
 * @see {@code URL}
 * @author Marc Siramy
 */
public class URLQuery implements Serializable {

	private static final long serialVersionUID = -8375339735573033078L;
	
	private Map<String, String> queryParameters;
	
	public URLQuery(URL url) {
		this.queryParameters = this.setQueryParameters(url.getQuery());
	}
	
	private Map<String, String> setQueryParameters(String s) {
	    String[] split = s.split("=|\\&");
	    int length = split.length;
	    Map<String, String> maps = new HashMap<>();
	    for (int i=0; i<length; i+=2){
	          maps.put(split[i], split[i+1]);
	     } 
	    return maps;
	}

	public Map<String, String> getQueryParameters() {
		return queryParameters;
	}
}

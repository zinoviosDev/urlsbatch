package fr.marcdev.urlsbatch.model;

import java.util.Set;

import lombok.Data;

@Data
public class URLQueriesIdsOccurrencesWrapper {
	
	private Set<String> ids;
	private Integer occurrences;

	public URLQueriesIdsOccurrencesWrapper(Set<String> ids, Integer occurrences) {
		this.ids = ids;
		this.occurrences = occurrences;
	}

}

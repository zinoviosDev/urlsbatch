package fr.marcdev.urlsbatch.service;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.marcdev.urlsbatch.config.ConsoleConstants;
import fr.marcdev.urlsbatch.model.URLQueriesIdsOccurrencesWrapper;

@Profile("!test")
@Service
public class MainService implements CommandLineRunner {
	
	@Autowired private BatchProcessService batchProcessService;
	@Autowired private UserInputService userIOService;
	
	@Value("${value.nb.with.highest.occur.to.find}")
	int valueNbWithHighestOccurencesToFind;

	public void run(String... args) throws Exception {
		// Informer l'utilisateur du traitement batch effectué par l'application
		System.out.println(ConsoleConstants.DEFAULT_START_MESSAGE);
		// Display prompt
		System.out.println(ConsoleConstants.INPUT_FOLDER_PATH_INSTRUCTIONS);
		// Récupérer le path du répertoire de fichiers logs saisi par l'utilisateur
		Path logDirPath = userIOService.getUserInput();
		if (logDirPath == null || logDirPath.toString() == null || logDirPath.toString() == "") {
			System.out.println(ConsoleConstants.ERR_DIR_INVALID);
		} else {
			// Map containing couples of (idValue, occurrence of idValue) not sorted
			Map<String, Integer> valuesOccurrences = this.batchProcessService
					.computeIdOccurrencesFromLogDirectory(logDirPath);
			List<URLQueriesIdsOccurrencesWrapper> urlQueriesIdsOccurrences = this.batchProcessService
					.getURLQueriesValuesWithHighestOccurrencesForIds(
							valueNbWithHighestOccurencesToFind, 
							valuesOccurrences);

			// Afficher tableau de résultats
			// qui contient les "ids\" qui ont le plus d'occurences\ndans des fichiers logs
			// d'Urls
			System.out.println(ConsoleConstants.RESULT_TABLE_HEADER);
			urlQueriesIdsOccurrences.forEach(elt -> {
				String keys = String.join(" ", elt.getIds());
				System.out.format(ConsoleConstants.RESULT_TABLE_ALIGN_FORMAT, keys, elt.getOccurrences());
			});
			System.out.println(ConsoleConstants.RESULT_TABLE_FOOTER);
		}
	}
}

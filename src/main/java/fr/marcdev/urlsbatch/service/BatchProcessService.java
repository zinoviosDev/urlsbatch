package fr.marcdev.urlsbatch.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.marcdev.urlsbatch.model.URLQueriesIdsOccurrencesWrapper;
import fr.marcdev.urlsbatch.model.URLQuery;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BatchProcessService {
	
	@Value("${batch.process.size}")
	int bufferSize;
	
	@Value("${urls.types.to.validate}")
	String[] schemes;

	public Map<String, Integer> computeIdOccurrencesFromLogDirectory(Path logDirPath) {
		// Map containing couples of (idValue, occurrence of idValue)
		Map<String, Integer> valuesOccurrences = new HashMap<>();

		// directory stream
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(logDirPath, "*.{log}")) {
			for (Path aFile : stream) {
				// read each file
				try (BufferedReader bfr = Files.newBufferedReader(aFile)) {
					// collection de values de l'id
					List<String> values = new ArrayList<>(bufferSize);
					for (String aUri; (aUri = bfr.readLine()) != null;) {
						UrlValidator urlValidator = new UrlValidator(schemes);
						if (!urlValidator.isValid(aUri)) {
							log.error("Bad url detected in log file : ", aUri);
						} else {
							// retreive "value" corresponding to "id" key of url, if any found (elsewhere
							// "value" is null)
							String idValue = new URLQuery(new URL(aUri)).getQueryParameters().get("id");
							if (idValue != null) {
								values.add(idValue);
							}
							if (values.size() == bufferSize) {
								buildValuesOccurenciesMap(values, valuesOccurrences);
								values.clear();
							}
						}
					}
					if (!values.isEmpty()) {
						buildValuesOccurenciesMap(values, valuesOccurrences);
					}
					bfr.close();
				}
			}
		} catch (IOException | DirectoryIteratorException x) {
			log.error("An error occured", x);
		}
		return valuesOccurrences;
	}

	
	public List<URLQueriesIdsOccurrencesWrapper> getURLQueriesValuesWithHighestOccurrencesForIds(
			int highestOccurrenceValuesNb,
			Map<String, Integer> valuesOccurrences) {
		List<URLQueriesIdsOccurrencesWrapper> resultList = new ArrayList<>();
		if (valuesOccurrences != null && valuesOccurrences.size() != 0) {
			List<Integer> highestValues = 
					valuesOccurrences.values().stream()
					.distinct()
					.sorted(Comparator.reverseOrder())
					.collect(Collectors.toList())
					.subList(0, highestOccurrenceValuesNb);
			highestValues.forEach(value -> {
				resultList.add(
					new URLQueriesIdsOccurrencesWrapper(
						getKeysByValue(valuesOccurrences, value), 
						value
					)
				);
			});
		}
		return resultList;
	}

	// increment occurrence in valuesOccurrence for each value found in values
	private void buildValuesOccurenciesMap(List<String> values, Map<String, Integer> valuesOccurrences) {
		for (String idValue : values) {
			if (valuesOccurrences.get(idValue) != null) {
				valuesOccurrences.put(idValue, valuesOccurrences.get(idValue) + 1);
			} else {
				valuesOccurrences.put(idValue, 1);
			}
		}
	}

	// get a Set of Map keys corresponding to a given value
	private static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
		return map.entrySet().stream().filter(entry -> Objects.equals(entry.getValue(), value)).map(Map.Entry::getKey)
				.collect(Collectors.toSet());
	}
}

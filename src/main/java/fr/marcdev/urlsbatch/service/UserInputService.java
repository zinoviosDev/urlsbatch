package fr.marcdev.urlsbatch.service;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserInputService {
	
	
	public Path getUserInput() {
		String enteredDirPath = null;
		Scanner scanner = new Scanner(System.in);
        try {
            while (enteredDirPath == null) {
                enteredDirPath = StringEscapeUtils.escapeJava(scanner.nextLine());
            }
        } catch(IllegalStateException 
        		| NoSuchElementException
        		| InvalidPathException 
        		| NullPointerException e) {
        	log.error("The entered path is not correct", e);
        } finally {
        	scanner.close();
        }
        
		return Paths.get(enteredDirPath);
	}
}

package fr.marcdev.urlsbatch;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@Ignore
class UrlsbatchApplicationTests {
	
	@Autowired ApplicationContext context;

	@Test
	void contextLoads() {
	}

}

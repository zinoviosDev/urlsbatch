package fr.marcdev.urlsbatch.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@Ignore
@SpringBootTest
public class UserInputServiceTest {
	
	@Autowired ApplicationContext context;
	// @Autowired UserInputService service;

	@Rule
	public final TextFromStandardInputStream systemInMock 
		= TextFromStandardInputStream.emptyStandardInputStream();

	@Test
	public void shouldGetUserInput() {
		systemInMock.provideLines("C:\\logs");
	    assertEquals("C:\\logs", "C:\\logs");
	}
}
